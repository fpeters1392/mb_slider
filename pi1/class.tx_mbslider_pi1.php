<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014  <>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

// require_once(PATH_tslib . 'class.tslib_pibase.php');

/**
 * Plugin 'Meinblog Parallax Slider' for the 'mb_slider' extension.
 *
 * @author	 <>
 * @package	TYPO3
 * @subpackage	tx_mbslider
 */
class tx_mbslider_pi1 extends tslib_pibase {
	public $prefixId      = 'tx_mbslider_pi1';		// Same as class name
	public $scriptRelPath = 'pi1/class.tx_mbslider_pi1.php';	// Path to this script relative to the extension dir.
	public $extKey        = 'mb_slider';	// The extension key.
	public $pi_checkCHash = TRUE;

	private $view;

	/**
	 * @var t3lib_db
	 */
	private $db;

	/**
	 * The main method of the Plugin.
	 *
	 * @param string $content The Plugin content
	 * @param array $conf The Plugin configuration
	 * @return string The content that is displayed on the website
	 */
	public function main($content, array $conf) {
		$this->conf = $conf;
		$this->pi_setPiVarDefaults();
		$this->pi_loadLL();

		$this->db = $GLOBALS['TYPO3_DB'];
		$this->initViewObject();
		$this->pi_initPIflexForm();
		$data = $this->cObj->data;
		$this->loadJsCssFiles($this->pi_getFFvalue($data['pi_flexform'], 'jQuery'), $this->pi_getFFvalue($data['pi_flexform'], 'fractionslider'));
		$data['slides'] = $this->loadSlides($this->pi_getFFvalue($data['pi_flexform'], 'storage'));
		return $this->pi_wrapInBaseClass($this->renderContent($data));
	}

	private function loadSlides($storagePid) {
		$fields = '*';
		$table = 'tx_mbslider_slides';
		$where = 'pid=' . intval($storagePid) . $this->cObj->enableFields($table);
		$this->db->store_lastBuiltQuery = 1;
		$res = $this->db->exec_SELECTquery($fields, $table, $where);
		$rows = array();
		while($row = $this->db->sql_fetch_assoc($res)) {
			$table = 'tx_mbslider_slide_elements';
			$where = 'uid IN (' . $row['elements'] . ')' . $this->cObj->enableFields($table);
			$orderBy = 'FIELD(uid, ' . $row['elements'] . ')';
			$resource = $this->db->exec_SELECTquery($fields, $table, $where, '', $orderBy);
			$elements = array();
			while($element = $this->db->sql_fetch_assoc($resource)) {
				$element = $this->getDamDataForContent($element, 'image', $table);
				$element['special'] = $element['special'] == 1? "data-special='cycle' " : "";
				$element['fixed'] = $element['fixed'] == 1 ? 'data-fixed ' : "";
				$element['step'] = $element['step'] == 0 ? "data-step='0' " : "data-step='" . $element['step'] . "'";
				$element['time_speed'] = $element['time_speed'] == 0 ? "" : "data-time='" . $element['time_speed'] . "'";
				$style = "style='";
				if($element['text_background'] != '' && $element['mode'] == 1) {
					$style .= "background-color: " . $element['text_background'] . "; ";
				}
				if($element['font_size'] != '' && $element['mode'] == 1) {
					$style .= "font-size: " . $element['font_size'] . "; ";
				}
				$style .= "'";
				$element['style'] = $style;
				$elements[] = $element;
			}
			$this->db->sql_free_result($resource);
			$row['elements'] = $elements;
			$rows[] = $row;
		}
		$this->db->sql_free_result($res);
		return $rows;
	}

	/**
	 * @param $loadjQuery
	 * @param $loadFractionSlider
	 */
	private function loadJsCssFiles($loadjQuery, $loadFractionSlider) {
		/**
		 * @var $pageRenderer t3lib_PageRenderer
		 */
		$pageRenderer = $GLOBALS['TSFE']->getPageRenderer();
		if($loadjQuery > 0) {
			$pageRenderer->addJsFooterFile('typo3conf/ext/mb_slider/Resources/Public/Jscript/jquery-1.11.1.min.js');
		}
		if($loadFractionSlider > 0) {
			$pageRenderer->addJsFooterFile('typo3conf/ext/mb_slider/Resources/Public/Jscript/jquery.fractionslider.min.js');
			$pageRenderer->addCssFile('typo3conf/ext/mb_slider/Resources/Public/Css/fractionslider.css');
		}
	}

	public function initViewObject() {
		$this->view = t3lib_div::makeInstance('Tx_Fluid_View_StandaloneView');
	}

	/**
	 * renderContent renders sie given Fluidtemplate an adds the given data to the view helper.
	 * Your templates have to be in "Resources/Private/Html/Content/"
	 *
	 * @param array $data Daten für das Fluid-Template
	 * @return string Content
	 */
	public function renderContent($data) {
		$content = '';
		$templateFile = t3lib_div::getFileAbsFileName('EXT:mb_slider/Resources/Private/Templates/') . 'Slider.html';
		if (file_exists($templateFile)) {
			$this->view->getRequest()->setControllerExtensionName($this->extKey);
			$this->view->setTemplatePathAndFilename($templateFile);
			$this->view->assign('data', $data);
			$this->view->assign('conf', $this->prepareFluidTs($this->conf));
			$content = $this->view->render();
		}

		return $content;
	}

	/**
	 * Just fetching the given DAM Records.
	 * If we are at a workspace draft, we will change the uid from where we have to fetch our records.
	 *
	 * @param array $data
	 * @param string $key
	 * @param string $table
	 * @return array
	 */
	public function getDamDataForContent($data, $key = 'tx_damttcontent_files', $table = 'tt_content') {
		$id = intval($data['uid']);
		//If we have a workspace draft and preview there, just change the UID =)
		if(isset($data['_ORIG_uid']) && $data['_ORIG_uid']) {
			$id = intval($data['_ORIG_uid']);
		}

		if(intval($data[$key]) > 0) {
			$data[$key] = tx_dam_db::getReferencedFiles($table, $id, $key, 'tx_dam_mm_ref', '', array(), '', 'sorting_foreign ASC');
			foreach ($data[$key]['rows'] AS $row) {
				$helper[] = $row;
			}
			$data[$key]['rows'] = $helper;
		}

		return $data;
	}
	/**
	 * We have to prepare our TypoScript Setup for Fluid because both are working with a "." notation.
	 * We are removing the dots from our TypoScript and preparing everything so we can use it in our Fluid-Template.
	 *
	 * @param array $array
	 * @return array
	 */
	protected function prepareFluidTs($array = array()) {
		$toLoop = count($array) > 0 ? $array : $this->conf['ts'];
		$helper = array();
		if( is_array($toLoop) && count($toLoop) > 0 ) {
			foreach( $toLoop AS $key => $value ) {
				$val = '';
				$key = str_replace( '.', '', $key);
				if(is_array($value)) {
					$value = $this->prepareFluidTs($value);
					if($helper[ $key ] != '') {
						$val = $helper[ $key ];
					}
				}
				$helper[ $key ] = $value;
				if($val != '') {
					$helper[ $key ]['value'] = $val;
				}
			}
		}
		return $helper;
	}

	/**
	 *
	 * @param $listType
	 * @return array flexform array
	 */
	protected function getFlexformArray() {
		$lConf = array();
		$piFlexForm = $this->cObj->data['pi_flexform'];
		if(is_array($piFlexForm['data'] ) && count($piFlexForm['data'] ) > 0) {
			$index = $GLOBALS['TSFE']->sys_language_uid;
			$sDef = current($piFlexForm['data']);
			$lDef = array_keys($sDef);
			foreach ( $piFlexForm['data'] as $sheet => $data ) {
				foreach ($data['lDEF'] as $key => $val ) {
					$lConf[$key] = $this->pi_getFFvalue($piFlexForm, $key, $sheet);
				}
			}
		}
		return $lConf;
	}
}



if (defined('TYPO3_MODE') && isset($GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/mb_slider/pi1/class.tx_mbslider_pi1.php'])) {
	include_once($GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/mb_slider/pi1/class.tx_mbslider_pi1.php']);
}

?>