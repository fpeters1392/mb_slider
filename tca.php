<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

$TCA['tx_mbslider_slides'] = array(
	'ctrl' => $TCA['tx_mbslider_slides']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'hidden,starttime,endtime,fe_group,label,elements'
	),
	'feInterface' => $TCA['tx_mbslider_slides']['feInterface'],
	'columns' => array(
		'hidden' => array(		
			'exclude' => 1,
			'label'   => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config'  => array(
				'type'    => 'check',
				'default' => '0'
			)
		),
		'starttime' => array(		
			'exclude' => 1,
			'label'   => 'LLL:EXT:lang/locallang_general.xml:LGL.starttime',
			'config'  => array(
				'type'     => 'input',
				'size'     => '8',
				'max'      => '20',
				'eval'     => 'date',
				'default'  => '0',
				'checkbox' => '0'
			)
		),
		'endtime' => array(		
			'exclude' => 1,
			'label'   => 'LLL:EXT:lang/locallang_general.xml:LGL.endtime',
			'config'  => array(
				'type'     => 'input',
				'size'     => '8',
				'max'      => '20',
				'eval'     => 'date',
				'checkbox' => '0',
				'default'  => '0',
				'range'    => array(
					'upper' => mktime(3, 14, 7, 1, 19, 2038),
					'lower' => mktime(0, 0, 0, date('m')-1, date('d'), date('Y'))
				)
			)
		),
		'fe_group' => array(		
			'exclude' => 1,
			'label'   => 'LLL:EXT:lang/locallang_general.xml:LGL.fe_group',
			'config'  => array(
				'type'  => 'select',
				'items' => array(
					array('', 0),
					array('LLL:EXT:lang/locallang_general.xml:LGL.hide_at_login', -1),
					array('LLL:EXT:lang/locallang_general.xml:LGL.any_login', -2),
					array('LLL:EXT:lang/locallang_general.xml:LGL.usergroups', '--div--')
				),
				'foreign_table' => 'fe_groups',
				'size' => 5,
				'minitems' => 0,
				'maxitems' => 150
			)
		),
		'label' => array(		
			'exclude' => 0,		
			'label' => 'LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slides.label',		
			'config' => array(
				'type' => 'input',	
				'size' => '30',
			)
		),
		'elements' => array(		
			'exclude' => 0,		
			'label' => 'LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slides.elements',		
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_mbslider_slide_elements',
				'minitems' => 1,
				'maxitems' => 15
			)
		),
	),
	'types' => array(
		'0' => array('showitem' => 'hidden;;1;;1-1-1, fe_group, label, elements')
	),
	'palettes' => array(
		'1' => array('showitem' => 'starttime, endtime')
	)
);



$TCA['tx_mbslider_slide_elements'] = array(
	'ctrl' => $TCA['tx_mbslider_slide_elements']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'hidden,label,mode,image,slide_text,in_animation,fixed,position_x,position_y,out_animation,ease_in,ease_out,time_speed,delay,step,special,text_background'
	),
	'feInterface' => $TCA['tx_mbslider_slide_elements']['feInterface'],
	'columns' => array(
		'hidden' => array(		
			'exclude' => 1,
			'label'   => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config'  => array(
				'type'    => 'check',
				'default' => '0'
			)
		),
		'label' => array(		
			'exclude' => 0,		
			'label' => 'LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.label',		
			'config' => array(
				'type' => 'input',	
				'size' => '30',
			)
		),
		'mode' => array(
			'exclude' => 0,		
			'label' => 'LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.mode',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.mode.I.0', '0'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.mode.I.1', '1'),
				),
				'size' => 1,	
				'maxitems' => 1,
			)
		),
		'image' => txdam_getMediaTCA('image_field', 'image'),
		'slide_text' => array(
			'exclude' => 0,		
			'label' => 'LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.slide_text',
			'config' => array(
				'type' => 'input',	
				'size' => '30',
			)
		),
		'in_animation' => array(
			'exclude' => 0,		
			'label' => 'LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.in_animation',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.in_animation.I.0', '0'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.in_animation.I.1', '1'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.in_animation.I.2', '2'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.in_animation.I.3', '3'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.in_animation.I.4', '4'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.in_animation.I.5', '5'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.in_animation.I.6', '6'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.in_animation.I.7', '7'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.in_animation.I.8', '8'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.in_animation.I.9', '9'),
				),
				'size' => 1,	
				'maxitems' => 1,
			)
		),
		'fixed' => array(		
			'exclude' => 0,		
			'label' => 'LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.fixed',		
			'config' => array(
				'type' => 'check',
			)
		),
		'position_x' => array(		
			'exclude' => 0,		
			'label' => 'LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.position_x',		
			'config' => array(
				'type'     => 'input',
				'size'     => '4',
				'max'      => '4',
				'eval'     => 'int',
				'checkbox' => '0',
				'default' => 0
			)
		),
		'position_y' => array(		
			'exclude' => 0,		
			'label' => 'LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.position_y',		
			'config' => array(
				'type'     => 'input',
				'size'     => '4',
				'max'      => '4',
				'eval'     => 'int',
				'checkbox' => '0',
				'default' => 0
			)
		),
		'out_animation' => array(
			'exclude' => 0,		
			'label' => 'LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.out_animation',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.in_animation.I.0', '0'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.in_animation.I.1', '1'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.in_animation.I.2', '2'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.in_animation.I.3', '3'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.in_animation.I.4', '4'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.in_animation.I.5', '5'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.in_animation.I.6', '6'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.in_animation.I.7', '7'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.in_animation.I.8', '8'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.in_animation.I.9', '9'),
				),
				'size' => 1,
				'maxitems' => 1,
			)
		),
		'ease_in' => array(		
			'exclude' => 0,		
			'label' => 'LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in',		
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.0', '0'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.1', '1'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.2', '2'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.3', '3'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.4', '4'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.5', '5'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.6', '6'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.7', '7'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.8', '8'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.9', '9'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.10', '10'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.11', '11'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.12', '12'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.13', '13'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.14', '14'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.15', '15'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.16', '16'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.17', '17'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.18', '18'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.19', '19'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.20', '20'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.21', '21'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.22', '22'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.23', '23'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.24', '24'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.25', '25'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.26', '26'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.27', '27'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.28', '28'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.29', '29'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.30', '30'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.31', '31'),
				),
				'size' => 1,
				'maxitems' => 1,
			)
		),
		'ease_out' => array(		
			'exclude' => 0,		
			'label' => 'LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_out',		
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.0', '0'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.1', '1'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.2', '2'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.3', '3'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.4', '4'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.5', '5'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.6', '6'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.7', '7'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.8', '8'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.9', '9'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.10', '10'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.11', '11'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.12', '12'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.13', '13'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.14', '14'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.15', '15'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.16', '16'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.17', '17'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.18', '18'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.19', '19'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.20', '20'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.21', '21'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.22', '22'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.23', '23'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.24', '24'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.25', '25'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.26', '26'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.27', '27'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.28', '28'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.29', '29'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.30', '30'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.ease_in.I.31', '31'),
				),
				'size' => 1,
				'maxitems' => 1,
			)
		),
		'time_speed' => array(
			'exclude' => 0,		
			'label' => 'LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.time_speed',
			'config' => array(
				'type'     => 'input',
				'size'     => '4',
				'max'      => '4',
				'eval'     => 'int',
				'checkbox' => '0',
				'default' => 0
			)
		),
		'delay' => array(		
			'exclude' => 0,		
			'label' => 'LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.delay',		
			'config' => array(
				'type'     => 'input',
				'size'     => '4',
				'max'      => '4',
				'eval'     => 'int',
				'checkbox' => '0',
				'default' => 0
			)
		),
		'step' => array(		
			'exclude' => 0,		
			'label' => 'LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.step',		
			'config' => array(
				'type'     => 'input',
				'size'     => '4',
				'max'      => '4',
				'eval'     => 'int',
				'checkbox' => '0',
				'default' => 0
			)
		),
		'special' => array(		
			'exclude' => 0,		
			'label' => 'LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.special',		
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.special.I.0', '0'),
					array('LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.special.I.1', '1'),
				),
				'size' => 1,	
				'maxitems' => 1,
			)
		),
		'text_background' => array(		
			'exclude' => 0,		
			'label' => 'LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.text_background',		
			'config' => array(
				'type' => 'input',	
				'size' => '30',
			)
		),
		'font_size' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:mb_slider/locallang_db.xml:tx_mbslider_slide_elements.font_size',
			'config' => array(
				'type' => 'input',
				'size' => '30',
			)
		),
	),
	'types' => array(
		'0' => array('showitem' => 'hidden;;1;;1-1-1, label, mode, image, slide_text, text_background, font_size, --div--;Animation Settings, in_animation, out_animation, fixed, position_x, position_y, ease_in, ease_out, time_speed, delay, step, special')
	),
	'palettes' => array(
		'1' => array('showitem' => '')
	)
);
?>