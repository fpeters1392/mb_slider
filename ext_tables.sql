#
# Table structure for table 'tx_mbslider_slides'
#
CREATE TABLE tx_mbslider_slides (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	sorting int(10) DEFAULT '0' NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,
	starttime int(11) DEFAULT '0' NOT NULL,
	endtime int(11) DEFAULT '0' NOT NULL,
	fe_group int(11) DEFAULT '0' NOT NULL,
	label tinytext,
	elements tinytext,
	
	PRIMARY KEY (uid),
	KEY parent (pid)
) ENGINE=InnoDB;



#
# Table structure for table 'tx_mbslider_slide_elements'
#
CREATE TABLE tx_mbslider_slide_elements (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
  sorting int(10) DEFAULT '0' NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,
	label tinytext,
	mode int(11) DEFAULT '0' NOT NULL,
	image tinytext,
	slide_text tinytext,
	in_animation int(11) DEFAULT '0' NOT NULL,
	fixed tinyint(3) DEFAULT '0' NOT NULL,
	position_x int(11) DEFAULT '0' NOT NULL,
	position_y int(11) DEFAULT '0' NOT NULL,
	out_animation int(11) DEFAULT '0' NOT NULL,
	ease_in tinytext,
	ease_out tinytext,
	time_speed int(11) DEFAULT '0' NOT NULL,
	delay int(11) DEFAULT '0' NOT NULL,
	step int(11) DEFAULT '0' NOT NULL,
	special int(11) DEFAULT '0' NOT NULL,
	text_background tinytext,
	font_size tinytext,

	PRIMARY KEY (uid),
	KEY parent (pid)
) ENGINE=InnoDB;