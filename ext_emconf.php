<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "mb_slider".
 *
 * Auto generated 14-06-2014 09:25
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'MB Slider',
	'description' => 'Meinblog Parallax Slider',
	'category' => '',
	'author' => 'Florian Peters',
	'author_email' => 'fpeters1392@googlemail.com',
	'shy' => '',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => '',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author_company' => '',
	'version' => '2.0.0',
	'constraints' => array(
		'depends' => array(
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:14:{s:9:"ChangeLog";s:4:"d3f0";s:12:"ext_icon.gif";s:4:"1bdc";s:17:"ext_localconf.php";s:4:"a98f";s:14:"ext_tables.php";s:4:"3e29";s:14:"ext_tables.sql";s:4:"6b72";s:35:"icon_tx_mbslider_slide_elements.gif";s:4:"475a";s:27:"icon_tx_mbslider_slides.gif";s:4:"475a";s:16:"locallang_db.xml";s:4:"8aa3";s:10:"README.txt";s:4:"ee2d";s:7:"tca.php";s:4:"988f";s:19:"doc/wizard_form.dat";s:4:"160a";s:20:"doc/wizard_form.html";s:4:"1493";s:29:"pi1/class.tx_mbslider_pi1.php";s:4:"c8df";s:17:"pi1/locallang.xml";s:4:"35da";}',
);

?>